# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 19:34:57 2018

@author: CEC
"""
from bokeh.embed import components
from bokeh.plotting import figure

label = {
    'out_invoice': 'Ventas',
    'in_invoice': 'Compras',
    'out_refund': 'Devoluciones en Ventas',
    'in_refund': 'Devoluciones en Compras',
    'mrp_consumption': 'Consumo de Producción',
    'mrp_production': 'Producción'
}


def make_plot(df, t):
    plot = figure(title='Resumen: {} de {}'.format(label[t],
                                                   df['name'].values[0]),
                  plot_height=300,
                  plot_width=1000,)
    plot.line(df['month'], df['total'], line_width=4, legend=label[t])

    script, div = components(plot)
    return script, div
