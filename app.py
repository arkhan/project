#!/usr/bin/env python

from datetime import datetime as dt

import pandas as pd
from main import make_plot
from flask import Flask, redirect, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)


class ProductHistory(db.Model):
    __tablename__ = 'product_history'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(20))
    name = db.Column(db.String(120))
    origin = db.Column(db.String(20))
    date = db.Column(db.DateTime)
    year = db.Column(db.String(4))
    month = db.Column(db.String(2))
    transaction_type = db.Column(db.String(20))
    qty = db.Column(db.Float)


@app.route('/', methods=['GET', 'POST'])
def index():
    product_data = []
    if request.method == 'POST':
        product_data = ProductHistory.query.filter(
            ProductHistory.code == request.form['product'].upper())
        return render_template('index.jinja2', product_data=product_data)
    return render_template('index.jinja2', product_data=product_data)


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        csv_file = request.files['file_data']
        csv_reader = pd.read_csv(csv_file.stream, delimiter=';')
        for index, row in csv_reader.iterrows():
            data = row.to_dict()
            if data.get('id', False):
                data.pop('id', None)
            data['date'] = dt.strptime(data['date'], '%Y-%m-%d %H:%M:%S')
            product_data = ProductHistory(**data)
            db.session.add(product_data)
            db.session.commit()
        return redirect(url_for('index'))
    product_data = ProductHistory.query.order_by(
        ProductHistory.code.asc()).all()
    return render_template('show.jinja2', product_data=product_data)


@app.route('/process', methods=['GET', 'POST'])
def process():
    plots = []
    if request.method == 'POST':
        for t in ['out_invoice', 'in_invoice', 'out_refund',
                  'in_refund', 'mrp_consumption', 'mrp_production']:
            query = ProductHistory.query.with_entities(
                ProductHistory.name,
                ProductHistory.transaction_type,
                ProductHistory.year,
                ProductHistory.month,
                func.sum(ProductHistory.qty).label('total')).order_by(
                ProductHistory.year,
                ProductHistory.month,
            ).group_by(
                ProductHistory.name,
                ProductHistory.transaction_type,
                ProductHistory.year,
                ProductHistory.month).filter(
                ProductHistory.code == request.form['product'].upper(),
                ProductHistory.transaction_type == t)
            df = pd.read_sql(
                query.statement, query.session.bind)
            if not df.empty:
                plots.append(make_plot(df, t))
        return render_template('analysis.jinja2', plots=plots)
    return render_template('analysis.jinja2', plots=plots)


if __name__ == '__main__':
    db.create_all()
    app.debug = True
    app.run()
